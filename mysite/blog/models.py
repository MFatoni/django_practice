from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from taggit.managers import TaggableManager


# Create your models here.
class PublishedManager(models.Manager):
	def get_queryset(self):
		return super(PublishedManager, self).get_queryset().filter(status='published')

class Post(models.Model):
	STATUS_CHOICES = (
		('draft', 'Draft'),
		('published', 'Published'),
	)
	title = models.CharField(max_length=250)
	slug = models.SlugField(max_length=250, unique_for_date='publish')
	author = models.ForeignKey(User, related_name='blog_posts',on_delete=models.CASCADE)
	# cascade = also delete child
	body = models.TextField()
	publish = models.DateTimeField(default=timezone.now)
	created =  models.DateTimeField(auto_now_add=True)
	update =  models.DateTimeField(auto_now=True)
	status =  models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')
	objects = models.Manager() #default manager
	published = PublishedManager() #costum manager
	tags = TaggableManager()

	class Meta:
		ordering = ('-publish',)

	# - descending

	def __str__(self):
		return self.title

	# to check sqlquery = python manage.py sqlmigrate blog 0001
	def get_absolute_url(self):        
		return reverse('blog:post_detail',args=[self.publish.year,self.publish.strftime('%m'),self.publish.strftime('%d'),self.slug])
	#blog:post_detail = using namespace in blog, add app_name if wanna use it

class Comment(models.Model):
	post = models.ForeignKey(Post, related_name='comments',on_delete=models.CASCADE)
	name = models.CharField(max_length=80)
	email = models.EmailField()
	body = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=True)

	class Meta:
		ordering = ('created',)

	def __str__(self):
		return 'Comment by {} on {}'.format(self.name, self.post)
